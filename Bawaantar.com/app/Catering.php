<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catering extends Model
{
	public $table = 'catering';
    protected $fillable = [
        'id', 'id_catering', 'id_hari', 'makanan_menu', 'harga_menu', 'deskripsi'
    ];
}
