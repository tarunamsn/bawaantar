<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hari extends Model
{
	public $table = 'catering_hari';
    protected $fillable = [
        'id_hari', 'hari'
    ];
}
