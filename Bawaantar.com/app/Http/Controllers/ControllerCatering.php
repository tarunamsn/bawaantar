<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Catering;

class ControllerCatering extends Controller {
    public function lihat_menu($hari){
    	$lihat = Catering::all();
    	return view("pilihhari/"."/pilih".$hari, ["lihat"=>$lihat]);
    }
}
