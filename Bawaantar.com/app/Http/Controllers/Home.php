<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
// use Illuminate\Html\HtmlServiceProvider;
use App\Marketplace;
// use Illuminate\Support\Facades\DB;

class Home extends Controller {
    public function home(){
    	$marketplace = Marketplace::all();
    	return view("index", ["lihat"=>$marketplace]);
    	// return var_dump($marketplace);
    }
}
