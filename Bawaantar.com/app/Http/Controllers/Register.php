<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\User;
use Auth;
class Register extends Controller {
    public function signup(){
    	return view("auth/register");
    }

    public function login(Request $request){
        if (Auth::attempt(array('email' => $request->email, 'password' => $request->password))) {
            echo "berhasil login";
        } else {
            echo "akun tidak terdaftar";
        }
        // dd('ds');
        // return view("auth/register");
    }

    public function signup_save(Request $request){
    	$save = User::create([
            'name'  => $request->name,
            'email'  => $request->email,
    		'password'	=> bcrypt($request->password)
    	]);
    	return back();
    }
}

?>