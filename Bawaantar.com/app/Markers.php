<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Markers extends Model
{
	public $table = 'markers';
    protected $fillable = [
        'id', 'name', 'address', 'lat', 'lng', 'type'
    ];
}
