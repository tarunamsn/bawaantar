<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marketplace extends Model
{
	public $table = 'marketplace';
    protected $fillable = [
        'id_catering', 'nama_catering', 'alamat_catering','kota_catering'
    ];
}
