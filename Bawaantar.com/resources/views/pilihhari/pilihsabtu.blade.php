<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BAWAANTAR.COM - Portal Catering Terbesar se-Indonesia</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">BAWAANTAR.COM</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Tentang Kami</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">LOGIN</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">DAFTAR SEKARANG</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <div class="col-lg-3">

          <h1 class="my-4">Hari</h1>
          <div class="list-group">
            <a href="{{ url('pesan/erangel/senin') }}" class="list-group-item">Senin</a>
            <a href="{{ url('pesan/erangel/selasa') }}" class="list-group-item">Selasa</a>
            <a href="{{ url('pesan/erangel/rabu') }}" class="list-group-item">Rabu</a>
            <a href="{{ url('pesan/erangel/kamis') }}" class="list-group-item">Kamis</a>
            <a href="{{ url('pesan/erangel/jumat') }}" class="list-group-item">Jumat</a>
            <a href="#" class="list-group-item">Sabtu</a>
            <a href="#" class="list-group-item">Minggu</a>
          </div>

        </div>

        <div class="col-lg-9">
        <h1>DAFTAR MENU HARI SABTU - ERANGEL CATERING</h1>

          <div class="row">

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a class="card-text">Ayam Kecap</a>
                  </h4>
                  <h5>Rp 10,000</h5>
                  <p class="card-text">Isi: ayam kecap, nasi, kerupuk putih, air minum gelas, timun</p>
                </div>
                <div class="card-footer">
                  <a href="#">PESAN</a>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a class="card-text">Udang Goreng Tepung</a>
                  </h4>
                  <h5>Rp 8,500</h5>
                  <p class="card-text">Isi: udang goreng tepung, nasi, kerupuk putih, air minum gelas, timun, sambal colek</p>
                </div>
                <div class="card-footer">
                  <a href="#">PESAN</a>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a class="card-text">Cumi Goreng Tepung</a>
                  </h4>
                  <h5>Rp 9,000</h5>
                  <p class="card-text">Isi: cumi goreng tepung, nasi, kerupuk putih, air minum gelas, timun, sambal colek</p>
                </div>
                <div class="card-footer">
                  <a href="#">PESAN</a>
                </div>
              </div>
            </div>

          </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; BAWAANTAR.COM 2019</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
