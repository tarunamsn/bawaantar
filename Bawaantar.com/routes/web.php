<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', 'Home@home');

Route::get('/signup', 'Register@signup');
Route::post('/signup', 'Register@signup_save');
Route::post('/login', 'Register@login');

Route::get('/signin', 'User@login');

Route::get('/', 'Home@home');

Route::get('/pesan/{hari}', 'ControllerCatering@lihat_menu');
Route::get('/pesan/{menuhari}/{hari}', 'DaftarMenu@lihat_menu');

// Route::get('/', function () {
//     return view('welcome');
// });
